package com.company;

import com.company.util.DateUtil;

import java.util.Scanner;

/**
 *
 * Classe que implementa un programa per fer fer servir utilitats relacionades amb les dates. Actualment només permet fer
 * servir les següents:
 *  - donat un número de mes, retorna el seu nom en català
 *  - donada una hora a Espanya retorna l'hora oficial al Japó
 *
 * @author José Luis Álvarez Casas
 * @version v1.0 (18/03/2021)
 * @see #main(String[])
 */
public class Main {

    /**
     *  Métode principal que mostra un menú en format de text, el qual actualment només mostra les següents opcions:
     *  1 - Nom del mes
     *  2 - Hora al Japó
     *  3 - Sortir (de l'aplicació)
     *
     *  Si l'usuari tria una de les dues primeres opcions, fa servir el métode de la clase DateUtil que
     *  implementa la funcionalitat corresponent.
     *
     *  El métode ignora el contingut del paràmetre "args".
     *
     * @see com.company.util.DateUtil#obteNomDelMes(int)
     * @see com.company.util.DateUtil#obteHoraDelJapo(int, int)
     * @see #obteOpcioDelMenu(Scanner)
     * @see #mostraAConsola(String)
     *
     * @param args s'ignora el seu contingut.
     *
     */
    public static void main(String[] args) {
        // Mostra el menú de l'aplicació
        Scanner consola = new Scanner(System.in);
        int opcio = obteOpcioDelMenu(consola);
        DateUtil dateUtil = new DateUtil();

        while (opcio != 3) {

            switch (opcio) {

                case 1:
                    // llegeix el número del mes
                    mostraAConsola("Introdueix un número de mes (1-12)");
                    int numDelMes = consola.nextInt();
                    // obté el nom del mes
                    String nomMes = dateUtil.obteNomDelMes(numDelMes);
                    mostraAConsola("El nom del mes és: " + nomMes);
                    break;
                case 2:
                    // llegeix l'hora
                    mostraAConsola("Introdueix l'hora a Espanya (0-23)");
                    int hora = consola.nextInt();
                    // llegeix els minuts
                    mostraAConsola("Introdueix els minuts de l'hora a Espanya (0-59)");
                    int minuts = consola.nextInt();
                    // calcula l'hora al Japó
                    String strHoraAJapo = dateUtil.obteHoraDelJapo(hora, minuts);
                    mostraAConsola("Al Japón són les " + strHoraAJapo);
                    break;
                default:
                    mostraAConsola("Opció no prevista, si us plau entreu una opcio entre [1-3]");
            }
            // Torna a mostrar el menú de l'aplicació
            opcio = obteOpcioDelMenu(consola);
        }
        mostraAConsola("Sortint del programa. Adéu!");
    }

    /**
     * Mostra per la "consola" el missatge passat per paràmetre.
     * @param missatge <code>String</code> amb el missatge a mostrar a la consola.
     */
    private static void mostraAConsola(String missatge) {
        System.out.println(missatge);
    }

    /**
     * Mostra el menú d'opcions de l'aplicació a la consola fent servir el métode <code>mostraAConsola</code>
     * i llegeix l'opció escollida per l'usuari amb el paràmetre Scanner
     * @param consola es fa servir per llegir de la consola l'opció triada per l'usuari
     * @return el número d'opció triat per l'usuari
     */
    private static int obteOpcioDelMenu(Scanner consola) {
        int opcio;
        mostraAConsola("Menú:");
        mostraAConsola("--------------");
        mostraAConsola("1. Nom del mes");
        mostraAConsola("2. Hora al Japó");
        mostraAConsola("3. Sortir");
        mostraAConsola("--------------");
        mostraAConsola("Entra la opció:");
        opcio = consola.nextInt();
        return opcio;
    }

}
